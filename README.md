# understanding-terraform-the-hard-way-with-kojitechs

## Step-01: Introduction
- Understand basic Terraform Commands
  - terraform init
  - terraform validate
  - terraform plan
  - terraform apply
  - terraform destroy


# Variable Data type
# """"""

# " " :> string
# [] = list 
# 80 = number 
# bool = true/false 
# {}  =  map 

# ### complicated
# [""] => list(string)
# [{}] => list(map)
# {[]} => map(list)
# """""

