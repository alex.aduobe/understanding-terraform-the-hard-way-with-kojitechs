
variable "vpc_cidr" {
  type = string
  description = "This vpc cidr"
  # default = "10.0.0.0/16" # you don't have to specify a cidr here. Rather, specify the cidr in the various env't .tfvars files
}