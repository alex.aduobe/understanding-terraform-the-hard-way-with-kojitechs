
terraform {
    required_version = ">= 1.1.0"
    backend "s3" {
        bucket = "underderstanding-terraform-the-hard-way-with-kojitechs1977" # THE SAME STATE BUCKET IN ALL PROJECT
        key = "path/env/workspace-with-single-account/" # CHANGE KEY
        region = "us-east-1"
        dynamodb_table = "terraform-lock"
        encrypt = true
    }
      
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


#####################################################
# DEPLOY VPC IN 3 DIFF ACCOUNTS 
#####################################################

resource "aws_vpc" "this" {
  cidr_block       = var.vpc_cidr # 10.0.0.0/16"  # var.vpc_cidr # THE VPCs SHOULD HAVE DIFF CIDRS
  instance_tenancy = "default"

  tags = {
    Name = upper("${terraform.workspace}-kojitechs-vpc") #SBX, DEV, PROD
  }
}
