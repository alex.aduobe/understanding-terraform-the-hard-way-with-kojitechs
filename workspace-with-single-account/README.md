## list workspaces
'''
terraform workspace list
```

## Create workspace
'''
terraform workspace new sbx
terraform workspace new prod
terraform workspace new dev
'''
## show specif workspace
'''
terraform workspace show
'''
## create the tfvars
'''sh
touch sbx.tfvars dev.tfvars prod.tfvars
'''
## switch workspace
'''
terraform workspace select dev
# NOTE
'''
## how do we run plan in this approach
``
terraform workspace show
terraform plan -var-file sbx.tfvars
``

## Unlock state file. Run
```
terraform force-unlock "COPY AND PASTE ID FROM TERMINAL UNDER LOCK INFO"
```