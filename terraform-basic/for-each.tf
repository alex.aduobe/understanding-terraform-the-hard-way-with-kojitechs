# Create 5 instances with windows AMI, 3 ubuntu ami. with the concept of for_each

# ["ami-017cdd6dc706848b2", "ami-08c40ec9ead489470"]
# count supports list
# for_each would support map



resource "aws_instance" "frontend" {
    count = 5

  ami           = ["ami-017cdd6dc706848b2", "ami-08c40ec9ead489470"]
  instance_type = "t3.micro"

  tags = {
    Name = "frontend"
  }
}




locals {
  frontend_instances = ""
}