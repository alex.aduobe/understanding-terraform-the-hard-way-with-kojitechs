
# # declaring local values
# # key: Anyname (local)
# # value: specifice value of local

locals {
  vpc_id = aws_vpc.kojitechs-vpc.id
  azs = data.aws_availability_zones.available.names
}

