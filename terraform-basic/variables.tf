# ## Variable block - It's like a place value. It's used to avaoid hard coding
# variable "ami_id" {
#     type = string 
#     description = "ami id"
#     default = "ami-09d3b3274b6c5d4aa"
# }

variable "vpc_cidr" {
  type = string
  description = "vpc cidr"
  default = "10.0.0.0/16"
}

variable "private_subnet_cidr"{
  description = "subnet cidr blocks"
  type        = list
  default     =["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24", "10.0.7.0/24"] 
}

variable "public_subnet_cidr"{
  description = "subnet cidr blocks"
  type        = list
  default     =["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
}

## How to create a map variable

## How to create a number variable