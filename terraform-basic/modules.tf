
# ## TYPES(ROOT/CHILD)
# # CLASSES OF MODULES( public / private modules)

# for specifi company(store github: private... modules())

# ## PUBLIC (VPC MODULE: )

data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = slice(data.aws_availability_zones.available.names, 0,3) # 3
  private_subnets = var.private_subnet_cidr
  public_subnets  = var.public_subnet_cidr

  enable_nat_gateway = false
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

## We want to get vpc id