
# # Output block-to get resource attribute
# output "private_dns" {
#   description = "private dns id"
#   value = aws_instance.ec2_instance.private_dns
# }


output "module_vpc_id" {
  value = module.vpc.vpc_id ##since we are using module we have to call the module the source  + resource name and the logical name.to get logical name go to main.tf of the root module
}

output "module_public_subnet_id" {
  value = module.vpc.public_subnets
}

## Whatever output you want make sure it's outputed in the root module first