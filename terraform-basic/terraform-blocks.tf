# Terraform Bblock
terraform {
      required_version = ">= 1.1.0"
    required_providers {
    aws = {
      source  = "hashicorp/aws" # ## Terraform Block-this block is meant to set constraint on terraform version
      version = "~> 4.0"
    }
  }
}

# ## Configure the AWS Provider
provider "aws" {
    region = "us-east-1"
    profile = "default" ## cat ~/.aws/credentials(run this command & you will see the credential details)
}

## resource_name          local_resource name
resource "aws_vpc" "kojitechs-vpc" {
  cidr_block = var.vpc_cidr # call/ reference the local
}


resource "aws_subnet" "private" {
    count = length(var.private_subnet_cidr) # 4

  vpc_id     = local.vpc_id # 
  cidr_block = var.private_subnet_cidr[count.index] # var.variable # count.index means I have this list, I want you to iterate over the list
  availability_zone =  element(slice(local.azs, 0,2), count.index) # No Good alternative: data.aws_availability_zones.available.names[0]
                                                                  # slice pulls 2 azs but element help distribute the subnets among 2 azs
  tags = {
    "Name" = "private_subnet"
  }
}


// won't work with this example
# output "private_subnet_without_count" {
#   value = aws_subnet.private.id // if your resource block didnt use count or for each, you can output using example: aws_subnet.private.id
# }

# legacy splat operator
output "private_subnet_with_count" {
  value = aws_subnet.private.*.id 
}

# latest splat operator - after TF 0.14 this approach is used. Will give all the subnets created
output "private_subnet_with_count__latest_splat" {
  value = aws_subnet.private[*].id 
}

# Single subnet id. the 0 will give you the 1st subnet id
output "private1_subnet_single_subnet_id" {
    value = aws_subnet.private1[0].id 
}


## RESOURCE BOCK # private_ip =
resource "aws_instance" "ec2_instance" {
  ami           = data.aws_ami.id # data_source(b/c it's available). # when we change region we don't need to change the ami
  instance_type = "t2.micro"

  tags = {
    "name" = "ec2_instance"
  }
}
# ##  
# resource "aws_subnet" "private2" {
#   vpc_id     = local.vpc_id # 
#   cidr_block = "10.0.3.0/24"
#   availability_zone =  data.aws_availability_zones.available.names[1]
# }

# ## 
# resource "aws_subnet" "private3" {
#   vpc_id     = alocal.vpc_id # 
#   cidr_block = "10.0.5.0/24"
#   availability_zone =  data.aws_availability_zones.available.names[0]
# }

# ## 
# resource "aws_subnet" "private4" {
#   vpc_id     = local.vpc_id # 
#   cidr_block = "10.0.7.0/24"
#   availability_zone =  data.aws_availability_zones.available.names[1]
# }

# ## data sources(to pull down attributes of existing resources (aws)



## Functions
### slice() # [1,2,3] => [1,2]
# Length - determines how many elements are in a list. ex: lenght(var.private_subnets_cidr)
# with ELEMENT you can say for the 1st subnet I want "us-east-1a", 2nd "us-east-1b", 3rd "us-east-1a" ,4th 3rd "us-east-1b", 5th "us-east-1a" etc
# Elementit's helps TF to iterate over the 2 azs
# count.index - helps to iterate over the list

##
## ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24", "10.0.7.0/24"] 
# ["us-east-1a", "us-east-1b"]

# element(slice(local.azs, 0,2), count.index)
# the SLICE function was used because we have a long list but we want only 2 out of the list


