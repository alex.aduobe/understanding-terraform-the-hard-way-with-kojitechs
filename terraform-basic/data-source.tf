
data "aws_availability_zone" "azs" {
  state = "available"
}


# # Declare the data source
data "aws_ami" "ami" {
  most_recent      = true
  owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"] # 2.0.20221103.3-x86_64. This is version and it can change so you can replace with *
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

}

## if you want to run only the data source block
## run: terraform apply -target data.aws_ami.ami
# after apply run: terraform console and then run: data.aws_ami.ami