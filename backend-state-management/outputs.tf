

output "bucket_name" {
  value = aws_s3_bucket.this[*].bucket # we want to call every bucket that get created so we have to use the legacy splat operator[*]
}