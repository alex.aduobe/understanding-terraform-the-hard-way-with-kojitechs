# To be able to use just a single resource block to create new S3 bucket when you need one, do the following: 
# Create a variable with type LIST, default should be a list of at least one bucket name with come at the end
# In the resouce block of the main.tf, bring in COUNT or FOR_EACH and get the LENGH of the variable: ex. length(var.bucket_name). The length will determine how many bucket we want to create
# Whenever you want to create a new bucket, go to the variable file, add the bucket name to the default and TF will take care of it

variable "bucket_name" {
  type = list
  description = "list of all buckets"
  default = [
    "underderstanding-terraform-the-hard-way-with-kojitechs1977",
    ]
}