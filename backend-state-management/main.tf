

terraform {
required_version = ">= 1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# NAME BUCKETC PER THE PROJECT NAME underderstanding-terradorm-the-hard-way-with-kojitech1977
##################################################################################
# CREATE AN S3 BUCKET FOR STATEFILE MANAGEMENT
##################################################################################

resource "aws_s3_bucket" "this" {
count = length(var.bucket_name)
    
  bucket = var.bucket_name[count.index]
  lifecycle {
    prevent_destroy = false
  }
}

###################################################################################
# Bucket policy that allows cross account access -- we will create this later during Terraform enterprise
###################################################################################


###################################################################################
# CREATE DYNAMODB TABLE
###################################################################################

resource "aws_dynamodb_table" "this" {
  name           = "terraform-lock"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
  lifecycle {
    prevent_destroy = false
  }

}