
terraform {

required_version = ">= 1.1.0"
    backend "s3" {
        bucket = "underderstanding-terraform-the-hard-way-with-kojitechs1977"
        key = "path/env" # use this path called path/env
        region = "us-east-1"
        dynamodb_table = "terraform-lock"
        encrypt = true
    }
        
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
