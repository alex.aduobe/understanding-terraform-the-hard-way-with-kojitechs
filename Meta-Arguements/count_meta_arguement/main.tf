locals {
  azs = data.aws_availability_zones.available.names
}


##################################################################################
# CREATING VPC
##################################################################################
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}


##################################################################################
# USING COUNT TO CREATE PUBLIC SUBNETS
##################################################################################
resource "aws_subnet" "public_subnet" {
    count = 2

  vpc_id     = aws_vpc.this.id
  cidr_block = var.public_cidr[count.index] # can't be duplicate
availability_zone = [local.azs[0],local.azs[1]][count.index]
map_publick_ip_on_launch = true

  tags = {
    Name = "public_subnet_${count.index + 1}" # using concatination
  }
}
##################################################################################
# USING COUNT TO CREATE PRIVATE SUBNETS
##################################################################################

resource "aws_subnet" "private_subnet" {
   count = 2

  vpc_id     = aws_vpc.this.id
  cidr_block = var.private_cidr[count.index] 
  availability_zone = slice(local.azs,0,2)[count.index] # #alternatively use: [local.azs[0], local.azs[1]][count.index]  slice(local.azs,0,2)[count.index]
  
 tags = {
    Name = "private_subnet_${count.index +1}" # # use concatenate sign ${} to make the name unique
  }
 }


##################################################################################
# USING COUNT TO CREATE DATEBASE SUBNET
##################################################################################
resource "aws_subnet" "database_subnet" {
   count = 2

  vpc_id     = aws_vpc.this.id
  cidr_block = var.database_cidr[count.index] # can't be duplicated
  availability_zone = [local.azs[0], local.azs[1]][count.index] #alternatively slice(local.azs,0,2)[count.index]
  
 tags = {
    Name = "database_subnet_${count.index +1}" # # use concatenate sign ${} to make the name unique
  }
 }

# you can use string format for the tags too. Check the terraform documention
# cp -R * and the destination

# EVEN NUMBER
public_cidr = [
  "10.0.0.0/24",
  "10.0.2.0/24",
  "10.0.4.0/24",
        6      ,
        8      ,
        10     ,
        12
]

private_cidr = [
  "10.0.1.0/24",
  "10.0.3.0/24",
  "10.0.5.0/24",
        7      ,
        9      ,
        11     ,
        13
]


database_cidr = [
  "10.0.51.0/24",
  "10.0.53.0/24",
  "10.0.55.0/24",
        57      ,
        59      ,
        61      ,
        63
]
