
##################################################################################
# USING FOR EACH TO CREATE PUBLIC SUBNETS
##################################################################################
locals {
  azs = data.aws_availability_zones.available.names #  public subnets. In the object called public-subnet, there are 2 keys. public_subnet_1, public_subnet_2

public_subnet = {
    public_subnet_1 = {
        cidr_block = "10.0.0.0/24"
        availability_zone = local.azs[0]
    }
    public_subnet_2 = {
        cidr_block = "10.0.2.0/24"
        availability_zone = local.azs[1]
    }
}
# public subnets. In the object called private-subnet, there are 2 keys. private_subnet_1, private_subnet_2
private_subnet = {
    private_subnet_1 = {
        cidr_block = "10.0.1.0/24"
        availability_zone = local.azs[0]
    }
    private_subnet_2 = {
        cidr_block = "10.0.3.0/24"
        availability_zone = local.azs[1]
    }
    }

  database_subnet = {
    database_subnet_1 = {
        cidr_block = "10.0.111.0/24"
        availability_zone = local.azs[0]
    }
    database_subnet_2 = {
        cidr_block = "10.0.113.0/24"
        availability_zone = local.azs[1]
    }
    }  
 
}

# the above map is called MAP OF MAP - very effective
############################################################################
# CREATING VPC
############################################################################

resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

###############################################################################
# USING FOR EACH TO CREATE PUBLIC SUBNETS                                       
###############################################################################
resource "aws_subnet" "public_subnet" {
   for_each = local.public_subnet # Iterate over the following keys: public_subnet_1, public_subnet_2

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block
  availability_zone = each.value.availability_zone
  map_public_ip_on_launch = true # ALWAYS ASSIGN AN IP

 tags = {
    Name = each.key 
  }
 }

###############################################################################
# CREATING EC2                                    
###############################################################################

 resource "aws_instance" "web" {
   ami = var.ami_id
   instance_type = "t3.micro"
   subnet_id = aws_subnet.public_subnet["public_subnet_2"].id

   tags = {
     "Name" = "Hello world"
   }
 }


###############################################################################
# USING FOR EACH TO CREATE PRIVATE SUBNETS                                       
###############################################################################
resource "aws_subnet" "private_subnet" {
   for_each = local.private_subnet

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value["cidr_block"] # there are 2 ways of calling keys. check the public subnet resource for the other method
  availability_zone = each["value.availability_zone"]
 
 tags = {
    Name = each.key # use concatenation sign ${} to make the name unique
  }
 }

###############################################################################
# USING FOR EACH TO CREATE DATABASE SUBNETS                                       
###############################################################################
resource "aws_subnet" "database_subnet" {
   for_each = var.database_subnet

  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block
  availability_zone = each.value.availability_zone
 
 tags = {
    Name = each.key # use concatenation sign ${} to make the name unique
  }
 }



## Last time we created MAP OBJECT in locals for the subnets now let's try VARIABLES
## Remember, when we are doing MODULES we have to structure it like this. We can't use locals
# In class we eventually went back to use the locals. See code at the top
 variable "private_subnet" {
  type = map
  description = "(optional) describe your variable"
  default = {
    private_subnet = {
      kojtechs_private_sub1 = {
        cidr_block = "10.0.1.0/24"
        availability_zone = "us-east-1a"
      }
       kojtechs_private_sub = {
        cidr_block = "10.0.3.0/24"
        availability_zone = "us-east-1b"
    }
  }
}
 }


 variable "database_subnet" {
  type = map
  description = "(optional) describe your variable"
  default = {
    database_subnet = {
      kojtechs_dabase_sub1 = {
        cidr_block = "10.0.51.0/24"
        availability_zone = "us-east-1a"
      }
      kojtechs_dabase_sub2 = {
        cidr_block = "10.0.53.0/24"
        availability_zone = "us-east-1b"
      }
       
  }
}
 }



## MAP OF LIST
# public_subnet_map_of_list = [
#   {
#     cidr_block = "10.0.0.0/24"
#     availability_zone = local.azs[0]
#   },
#   {
#     cidr_block = "10.0.0.0/24"
#     availability_zone = local.azs[0]
#   }
# ]

