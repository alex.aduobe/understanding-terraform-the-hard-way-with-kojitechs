###################################################################
# OUTPUT A SPECIFIC PUBLIC SUBNETS  IDS
###################################################################
output "public_subnet1_id" {
  value       = aws_subnet.public_subnet["public_subnet_1"].id
}

###################################################################
# OUTPUT ALL PUBLIC SUBNET IDS IN FOR_EACH
###################################################################
output "all_public_subnet_ids" {
    value = [for sub in aws_subnet.public_subnet: sub.id] # works for both count and for each. We are iterationg over the keys: public_subnet_1 & 2
}                                                                  # aws_subnet.private_subnet[*].id --accepted only for count