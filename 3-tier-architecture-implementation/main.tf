






terraform {
    required_version = ">= 1.1.0"

    backend "s3"  {
        bucket = "understanding-terraform-the-hard-way-with-kojitechs1977"
        key = "path/env"
        region = "us-east-1"
        dynamodb_table = "terraform-lock"
        encrypt = true 
    }
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

######################################################################
# PROVIDER BLOCK
######################################################################

provider "aws" {
    region = "us-east-1"
    # profile = "" 

# this tag will appear on all the resourses that we will create
    default_tags {
      tags = {
        ChangeCode = 1002323
        component = var.component
      }
    }
}

#######################################################################
# LOCAL BLOCK
#######################################################################

locals {
  vpc_id = aws_vpc.this.id
}


#######################################################################
# CREATING VPC
#######################################################################

resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tag = {
    Name = "kojitechs-vpc"
  }
}

#######################################################################
# CREATING INTERNET GATEWAY
#######################################################################

resource "aws_internet_gateway" "gw"{
vpc_id = aws_vpc.this.id
  
tags = {
  "Name" = "kojitechs-vpc-igw"
}

}

#####################################################################
# CREATING PUBLIC SUBNETS USING COUNT
######################################################################
resource "aws_subnet" "public_sunet" {
    count = 2

    vpc_id = local.vpc_id
    cidr_block = var.public_subnetcidr[count.index]
    availability_zone = [count.index]
    map_public_ip_on_launch = true
    
    tags = {
        Name = "Mani"
    }
}

#####################################################################
# CREATING PRIVATE SUBNETS USING COUNT
######################################################################

resource "aws_subnet" "private_sunet" {
    count = 2

    vpc_id = local.vpc_id
    cidr_block = [count.index]
    availability_zone = [count.index]
  
    
    tags = {
        Name = "Main"
    }
}
